import {
  getCircleProps, getDPath,
  getIntensityLabelProps,
  getIntensityLabels,
  getLeftX,
  getLeftY,
  getRightX,
  getRightY,
} from './WindRose';
import React from 'react';
import renderer from 'react-test-renderer';
import WindRose from './WindRose';


const defaultProps = {
  data: {
    max_avg_speed: 30.23134234,
    max_intensity: 0.1521,
    results: [ ...Array(36).keys() ].map(key => key * 10).reduce((acc, key) => {
      acc[key] = {
        intensity: key,
        velocity: key,
      };
      return acc;
    }, {}),
  },
};

describe('WindRose', () => {
  it('renders component with scale', () => {
    const headers = renderer.create(<WindRose { ...defaultProps }/>);
    const tree = headers.toJSON();
    expect(tree.props.className).toEqual('wr-WindRose');
    expect(tree.children[0].type).toEqual('svg');
    expect(tree.children[0].props.className).toEqual('wr-WindRose--graphics');
    expect(tree.children[1].type).toEqual('p');
    expect(tree.children[1].props.className).toEqual('wr-WindRose--text');
    expect(tree.children[1].children[0]).toEqual('Wind Rose');
  });

});

describe('.getIntensityLabels', () => {
  it('returns an array of percentages breaking the max intensity into 6 bands', () => {
    expect(getIntensityLabels({ max_intensity: 0.15 })).toEqual([ '15.00 %', '12.50 %', '10.00 %', '7.50 %', '5.00 %', '2.50 %' ]);
  });
});

describe('.getIntensityLabelProps', () => {
  it('returns props on where to place the intensity label', () => {
    expect(getIntensityLabelProps(10, 50, 40)).toEqual({
      r: 50,
      x: 285,
      y: 365,
      key: '50--text',
    });
  });
});

describe('.getCircleProps', () => {
  it('returns props of where to draw each circle', () => {
    expect(getCircleProps(50)).toEqual({
      r: 50,
      cx: 275,
      cy: 275,
      key: '50--circle',
    });
  });
});

describe('coordinates', () => {

  describe('.getLeftX', () => {
    it('computes the left x endpoint of the slice', () => {
      expect(getLeftX(50, .46)).toEqual(354.8857126027847);
    });
  });

  describe('.getRightX', () => {
    it('computes the right x endpoint of the slice', () => {
      expect(getRightX(50, .46)).toEqual(342.5953040136344);
    });
  });

  describe('.getLeftY', () => {
    it('computes the left y endpoint of the slice', () => {
      expect(getLeftY(50, .46)).toEqual(357.72407703894487);
    });
  });

  describe('.getRightY', () => {
    it('computes the right y endpoint of the slice', () => {
      expect(getRightY(50, .46)).toEqual(368.036954353119);
    });
  });
});

describe('.getDPath', () => {
  it('traces the slice', () => {
    expect(getDPath(1,2, 3,5)).toEqual(["M 275 275", "L 1 2", "A 180 180 0 0 1 3 5", "Z"])
  })
});