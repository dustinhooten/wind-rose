import React from 'react';
import renderer from 'react-test-renderer';
import WindSpeedKey, { getMaxAvgSpeed, getMaxSpeedProps, getMinSpeedProps } from './WindSpeedKey';

const defaultProps = {
  data: {
    max_avg_speed: 30.23134234,
  },
};
describe('WindSpeedKey', () => {

  it('renders component with scale', () => {
    const headers = renderer.create(<WindSpeedKey { ...defaultProps }/>);
    const tree = headers.toJSON();
    expect(tree.props.className).toEqual('wr-WindSpeedKey');
    expect(tree.children[0].type).toEqual('svg');
    expect(tree.children[0].props.className).toEqual('wr-WindSpeedKey--graphics');
    expect(tree.children[1].type).toEqual('p');
    expect(tree.children[1].props.className).toEqual('wr-WindSpeedKey--text');
    expect(tree.children[1].children[0]).toEqual('Wind Speed Key');
  });

});

describe('.getMaxAvgSpeed', () => {
  it('returns max avg speed string in m/s rounded to 2 decimal points', () => {
    expect(getMaxAvgSpeed(defaultProps.data)).toEqual('30.23 m/s');
  });

  it('returns 0.00 m/s when data in invalid', () => {
    expect(getMaxAvgSpeed({ max_avg_speed: NaN })).toEqual('0.00 m/s');
  });
});

describe('.getMaxSpeedProps', () => {
  it('returns the correct props', () => {
    expect(getMaxSpeedProps(50, 25, 10, 15)).toEqual({
      className: 'max-speed',
      x: 60,
      y: 40,
    });
  });
});

describe('.getMinSpeedProps', () => {
  it('returns the correct props', () => {
    expect(getMinSpeedProps(50, 25, 10, 15, 100)).toEqual({
      className: 'min-speed',
      x: 60,
      y: 110,
    });
  });
});
