import React from 'react';
import renderer from 'react-test-renderer';
import DataContainer from './DataContainer';

const defaultProps = {
  startDate: new Date(2019, 0, 1),
  endDate: new Date(2019, 1, 28),
};

describe('DataContainer', () => {
  test('it renders the component', () => {
    const headers = renderer.create(<DataContainer { ...defaultProps }/>);
    const tree = headers.toJSON();
    expect(tree.props.className).toEqual('wr-DataContainer');
  });

  test('it falls back to loading component while fetching data', () => {
    const headers = renderer.create(<DataContainer { ...defaultProps }/>);
    const tree = headers.toJSON();
    expect(tree.children[0].props.className).toMatch('wr-Loading');
  });
});