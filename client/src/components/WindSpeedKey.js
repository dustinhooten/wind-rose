import React from 'react';
import PropTypes from 'prop-types';
import './WindSpeedKey.css';
import { round } from './Utils';

export const getMaxAvgSpeed = data =>
  `${ round(data.max_avg_speed) } m/s`;

export const getMaxSpeedProps = (x, y, xOffset, yOffset) => ({
  className: 'max-speed',
  x: x + xOffset,
  y: y + yOffset,
});

export const getMinSpeedProps = (x, y, xOffset, yOffset, barHeight) => ({
  className: 'min-speed',
  x: x + xOffset,
  y: y + barHeight - yOffset,
});

const x = 275;
const y = 25;
const xOffset = 75;
const yOffset = 10;
const barHeight = 500;
const barWidth = 50;
const svgSize = 550;
const maxVelocityColor = '#042733';
const minVelocityColor = '#ffffff';

const displayName = 'wr-WindSpeedKey';
export default function WindSpeedKey({ data }) {

  const maxSpeedProps = getMaxSpeedProps(x, y, xOffset, yOffset);
  const minSpeedProps = getMinSpeedProps(x, y, xOffset, yOffset, barHeight);

  return (
    <div className={ displayName }>
      <svg width={ svgSize } height={ svgSize } className={ `${ displayName }--graphics` }>
        <defs>
          <linearGradient id="wind-key-gradient" gradientTransform="rotate(90)">
            <stop offset="0%" stopColor={ maxVelocityColor }/>
            <stop offset="100%" stopColor={ minVelocityColor }/>
          </linearGradient>
        </defs>
        <rect x={ x } y={ y } width={ barWidth } height={ barHeight } fill="url('#wind-key-gradient')"/>
        <text { ...maxSpeedProps }>{ getMaxAvgSpeed(data) }</text>
        <text { ...minSpeedProps }>0.00 m/s</text>
      </svg>
      <p className={ `${ displayName }--text` }>Wind Speed Key</p>
    </div>
  );

}

WindSpeedKey.propTypes = {
  data: PropTypes.object.isRequired,
};