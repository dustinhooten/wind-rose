import React from 'react';
import Headers from './Headers';
import renderer from 'react-test-renderer';

describe('Headers', () => {
  it('renders component', () => {
    const headers = renderer.create(<Headers/>);
    const tree = headers.toJSON();
    expect(tree.props.className).toEqual('wr-header');
  });

  it('renders component children', () => {
    const headers = renderer.create(<Headers/>);
    const children = headers.toJSON().children;
    children.forEach(child => {
      expect(child.props.className).toMatch(/wr-header--/);
    });
  });
});
