import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import Data from './Data';
import Loading from './Loading';
import './DataContainer.css';

const displayName = 'wr-DataContainer';
export default function DataContainer({ startDate, endDate }) {
  const dataProps = { startDate, endDate };
  return (
    <div className={ displayName }>
      <Suspense fallback={ <Loading/> }>
        <Data { ...dataProps }/>
      </Suspense>
    </div>
  );
}

DataContainer.propTypes = {
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
};