import { formatDate, getDegrees, pad, round, toRadians } from './Utils';

describe('.round', () => {
  it('rounds to 2 digits and returns a string', () => {
    expect(round(324.32231)).toEqual('324.32');
  });

  it('returns 0.00 when given NaN', () => {
    expect(round(NaN)).toEqual('0.00');
  });
});

describe('.toRadians', () => {
  it('converts degrees to radians', () => {
    expect(toRadians(45)).toEqual(0.7853981633974483);
  });
});

describe('.pad', () => {
  it('returns the given number as a string if it has two digits', () => {
    expect(pad(21)).toEqual('21');
  });
  it('returns the given number as a string padded with 0 if it has one digit', () => {
    expect(pad(1)).toEqual('01');
  });
});

describe('.formatDate', () => {
  it('returns a date formatted like YYYY-MM-DD', () => {
    expect(formatDate(new Date(2019, 1, 3))).toEqual('2019-02-03');
  });
});

describe('.getDegrees', () => {
  it('returns an array of degrees from 0 to 350 with intervals of 10', () => {
    const degrees = getDegrees();
    expect(degrees.length).toEqual(36);
    expect(degrees[0]).toEqual(0);
    expect(degrees[35]).toEqual(350);
  });
});