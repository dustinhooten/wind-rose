import React from 'react';
import PropTypes from 'prop-types';
import useFetch from 'fetch-suspense';
import WindRose from './WindRose';
import WindSpeedKey from './WindSpeedKey';
import './Data.css';
import { formatDate } from './Utils';

export const getUrl = (startDate, endDate) => {
  const startDateStr = formatDate(startDate);
  const endDateStr = formatDate(endDate);
  return `winds?start_date=${ startDateStr }&end_date=${ endDateStr }`;
};

export const getParams = () => ({
  accept: 'application/json',
});

const displayName = 'wr-Data';
export default function Data({ startDate, endDate }) {
  const url = getUrl(startDate, endDate);
  const data = useFetch(url, getParams());
  const windProps = { data };
  return (
    <div className={ displayName }>
      <WindRose { ...windProps }/>
      <WindSpeedKey { ...windProps }/>
    </div>
  );
}

Data.propTypes = {
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
};