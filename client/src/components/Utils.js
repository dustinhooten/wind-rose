export const round = float => {
  const number = Number.parseFloat(float);
  if (Number.isNaN(number)) {
    return '0.00';
  }
  return number.toFixed(2).toString();
};

export const toRadians = degree => degree * Math.PI / 180;

export const pad = num => {
  const dateStr = num.toString();
  return dateStr.length === 2 ? dateStr : '0' + dateStr;
};
export const formatDate = date => {
  const year = date.getYear() + 1900;
  const month = pad(date.getMonth() + 1);
  const day = pad(date.getDate());
  return `${ year }-${ month }-${ day }`;
};
export const getDegrees = () => [ ...Array(36).keys() ].map(key => {
  return key * 10;
});