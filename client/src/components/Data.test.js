import React, { Suspense } from 'react';
import { getParams, getUrl } from './Data';
import renderer from 'react-test-renderer';
import Data from './Data';

const defaultProps = {
  startDate: new Date(2019, 0, 1),
  endDate: new Date(2019, 1, 28),
};

describe('Data', () => {
  it('fallsback until the data is fetched', () => {
    const headers = renderer.create(
      <Suspense fallback={ <p className="Fallback">Loading</p> }>
        <Data { ...defaultProps }/>
      </Suspense>,
    );
    const tree = headers.toJSON();
    expect(tree.props.className).toEqual('Fallback');
    expect(tree.type).toEqual('p');
  });
});

describe('.getUrl', () => {
  const startDate = new Date(2019, 0, 1);
  const endDate = new Date(2019, 1, 28);
  it('returns the relative url with the start and end date params correctly formatted', () => {
    expect(getUrl(startDate, endDate)).toEqual('winds?start_date=2019-01-01&end_date=2019-02-28');
  });
});

describe('.getParams', () => {
  it('configures the call correctly', () => {
    expect(getParams()).toEqual({ accept: 'application/json' });
  });
});