import React from 'react';
import { Header } from 'semantic-ui-react';
import './Headers.css';

export default function Headers() {
  return (
    <div className="wr-header">
      <Header as="h2" className="wr-header--subtitle">Wind speed, direction, & intensity</Header>
      <Header as="h1" className="wr-header--title">Berthoud Pass, Colorado</Header>
      <Header as="h2" className="wr-header--date">2016-2019</Header>
    </div>
  );
}