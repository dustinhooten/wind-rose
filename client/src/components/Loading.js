import React from 'react';
import { Loader } from 'semantic-ui-react';

const displayName = 'wr-Loading';
export default function Loading() {
  const loaderProps = {
    active: true,
    className: displayName,
    disabled: false,
    indeterminate: true,
    inline: true,
    inverted: true,
    size: 'massive',
  };
  return <Loader { ...loaderProps }>Loading...</Loader>;
}