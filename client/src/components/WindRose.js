import React from 'react';
import PropTypes from 'prop-types';
import './WindRose.css';
import { getDegrees, round, toRadians } from './Utils';

const maxRadius = 250;
const center = 275;
const circleRadii = [ maxRadius, 200, 150, 100, 50, 1 ];
const sliceOffset = 4;
const svgSize = 550;
const yOffset = 20;
const xOffset = -10;

export const getIntensityLabels = data => {
  const maxIntensity = data.max_intensity;
  const increments = maxIntensity / 6.0;
  return [ 6, 5, 4, 3, 2, 1 ].map(i => round(increments * i * 100) + ' %');
};

export const getIntensityLabelProps = (xOffset, radius, yOffset) => ({
  x: center + xOffset,
  y: center + radius + yOffset,
  r: radius,
  key: radius + '--text',
});

export const getCircleProps = radius => ({
  cx: center,
  cy: center,
  r: radius,
  key: radius + '--circle',
});

const displayName = 'wr-WindRose';

export const getLeftX = (degree, currentIntensity) =>
  Math.cos(toRadians(degree - sliceOffset)) * maxRadius * currentIntensity + center;

export const getLeftY = (degree, currentIntensity) =>
  Math.sin(toRadians(degree - sliceOffset)) * maxRadius * currentIntensity + center;

export const getRightX = (degree, currentIntensity) =>
  Math.cos(toRadians(degree + sliceOffset)) * maxRadius * currentIntensity + center;

export const getRightY = (degree, currentIntensity) =>
  Math.sin(toRadians(degree + sliceOffset)) * maxRadius * currentIntensity + center;

export const getDPath = (leftX, leftY, rightX, rightY) => [
  `M ${ center } ${ center }`,
  `L ${ leftX } ${ leftY }`,
  `A 180 180 0 0 1 ${ rightX } ${ rightY }`,
  `Z`,
];

export default function WindRose({ data }) {

  const maxIntensity = data.max_intensity;
  const maxVelocity = data.max_avg_speed;
  const intensityLabels = getIntensityLabels(data);
  const degrees = getDegrees();

  return (
    <div className={ displayName }>
      <svg width={ svgSize } height={ svgSize } className={ `${ displayName }--graphics` }>

        { circleRadii.map(radius => {
          const circleProps = getCircleProps(radius);
          return (<circle { ...circleProps } />);
        }) }

        { circleRadii.map((radius, index) => {
          const textProps = getIntensityLabelProps(xOffset, radius, yOffset);
          return <text { ...textProps } >{ intensityLabels[index] }</text>;
        }) }

        { degrees.map(degree => {
          const { intensity, velocity } = data.results[degree];
          const currentIntensity = intensity / maxIntensity;
          const currentVelocity = velocity / maxVelocity;
          const leftX = getLeftX(degree, currentIntensity);
          const leftY = getLeftY(degree, currentIntensity);
          const rightX = getRightX(degree, currentIntensity);
          const rightY = getRightY(degree, currentIntensity);
          const pathProps = {
            d: getDPath(leftX, leftY, rightX, rightY),
            fillOpacity: currentVelocity,
          };
          return <path { ...pathProps } key={ degree }/>;
        }) }
      </svg>
      <p className={ `${ displayName }--text` }>Wind Rose</p>
    </div>
  );
}

WindRose.propTypes = {
  data: PropTypes.object.isRequired,
};