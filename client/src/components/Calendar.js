import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './Calendar.css';

const displayName = 'wr-Calendar';
export default function Calendar({ startDate, setStartDate, endDate, setEndDate }) {

  const dateFormat = 'MMMM d, yyyy';
  const fromMinDate = new Date(2016, 2, 1);
  const toMinDate = new Date(2016, 2, 2);
  const fromMaxDate = new Date(2019, 1, 27);
  const toMaxDate = new Date(2019, 1, 28);

  const fromProps = {
    className: `${ displayName }--from`,
    dateFormat,
    endDate,
    maxDate: fromMaxDate,
    minDate: fromMinDate,
    onChange: setStartDate,
    selected: startDate,
    selectsStart: true,
    startDate,
  };

  const toProps = {
    className: `${ displayName }--to`,
    dateFormat,
    endDate,
    maxDate: toMaxDate,
    minDate: toMinDate,
    onChange: setEndDate,
    selected: endDate,
    selectsEnd: true,
    startDate,
  };


  return (
    <div className={ displayName }>
      <div className={ `${ displayName }--fromWrapper` }>
        <span>From</span>
        <DatePicker { ...fromProps }/>
      </div>
      <div className={ `${ displayName }--toWrapper` }>
        <span>To</span>
        <DatePicker { ...toProps }/>
      </div>
    </div>
  );
}

Calendar.propTypes = {
  startDate: PropTypes.instanceOf(Date).isRequired,
  setStartDate: PropTypes.func.isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
  setEndDate: PropTypes.func.isRequired,
};