import React from 'react';
import Calendar from './Calendar';
import renderer from 'react-test-renderer';

const defaultProps = {
  startDate: new Date(2019, 0, 1),
  endDate: new Date(2019, 1, 28),
  setStartDate: () => {
  },
  setEndDate: () => {
  },
};
describe('Calendar', () => {
  it('renders component', () => {
    const headers = renderer.create(<Calendar { ...defaultProps }/>);
    const tree = headers.toJSON();
    expect(tree.props.className).toEqual('wr-Calendar');
  });

  it('renders component children with the correct values', () => {
    const headers = renderer.create(<Calendar { ...defaultProps }/>);
    const children = headers.toJSON().children;
    const fromDiv = children[0];
    const toDiv = children[1];
    const fromLabel = fromDiv.children[0];
    const startDatePicker = fromDiv.children[1];
    const toLabel = toDiv.children[0];
    const endDatePicker = toDiv.children[1];
    expect(fromLabel.type).toEqual('span');
    expect(fromLabel.children[0]).toEqual('From');
    expect(startDatePicker.type).toEqual('div');
    expect(startDatePicker.props.className).toEqual('react-datepicker-wrapper');
    expect(startDatePicker.children[0].children[0].props.value).toEqual('January 1, 2019');
    expect(toLabel.type).toEqual('span');
    expect(toLabel.children[0]).toEqual('To');
    expect(endDatePicker.type).toEqual('div');
    expect(endDatePicker.props.className).toEqual('react-datepicker-wrapper');
    expect(endDatePicker.children[0].children[0].props.value).toEqual('February 28, 2019');
  });

});
