import React, { useState } from 'react';
import './App.css';
import Headers from './Headers';
import Footer from './Footer';
import Calendar from './Calendar';
import DataContainer from './DataContainer';

export default function App() {

  const [ startDate, setStartDate ] = useState(new Date(2019, 1, 27));
  const [ endDate, setEndDate ] = useState(new Date(2019, 1, 28));

  const calendarProps = { startDate, setStartDate, endDate, setEndDate };
  const dataProps = { startDate, endDate };

  return (
    <div className="App">
      <Headers/>
      <DataContainer {...dataProps}/>
      <Calendar { ...calendarProps }/>
      <Footer/>
    </div>
  );
}