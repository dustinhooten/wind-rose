import React from 'react';
import './Footer.css';

export default function Footer() {
  return (
    <div className="wr-Footer">
      <p className="wr-Footer--text">&copy; 2019 Dustin Hooten - dustinhooten@gmail.com - Data Visualization Example</p>
    </div>
  );
}