import React from 'react';
import Footer from './Footer';
import renderer from 'react-test-renderer';

describe('Footer', () => {
  it('renders component', () => {
    const headers = renderer.create(<Footer/>);
    const tree = headers.toJSON();
    expect(tree.props.className).toEqual('wr-Footer');
  });
});
