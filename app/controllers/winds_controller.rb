class WindsController < ApplicationController
  def index
    begin
      start_date = parse_date(params[:start_date])
      end_date   = parse_date(params[:end_date])
    rescue StandardError
      return render :nothing => true, :status => :bad_request
    end

    @winds = Wind.in_date_range(start_date, end_date)
    render json: @winds
  end

  private

    def parse_date(date_str)
      DateTime.strptime(date_str, '%Y-%m-%d')
    end
end
