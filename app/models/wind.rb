class Wind < ApplicationRecord


  scope :after, ->(start_date) { where('datetime >= ?', start_date) }
  scope :before, ->(end_date) { where('datetime <= ?', end_date) }
  scope :by_direction, ->() { group(:direction) }
  scope :velocities, ->() { by_direction.average(:speed) }
  scope :intensities, ->() { by_direction.count }

  def self.in_date_range(start_date, end_date)
    observations       = Wind.after(start_date).before(end_date)
    total_observations = observations.count
    velocities         = observations.velocities
    intensities        = observations.intensities
    avg_speeds         = velocities.values
    max_avg_speed      = avg_speeds.max
    min_avg_speed      = avg_speeds.min
    intensity          = intensities.values
    max_intensity      = intensity.max / total_observations.to_d
    min_intensity      = intensity.min / total_observations.to_d
    results            = merge(velocities, intensities, total_observations)

    { total_observations: total_observations,
      max_avg_speed:      max_avg_speed,
      min_avg_speed:      min_avg_speed,
      max_intensity:      max_intensity,
      min_intensity:      min_intensity,
      results:            results }
  end

  private

    def self.merge(velocities, intensities, total_observations)
      all_directions = (0..350).step(10)

      Hash[all_directions.map { |direction|
        [direction, { velocity:  (velocities[direction] || 0).to_d,
                      intensity: (intensities[direction] || 0) / total_observations.to_d }] }]
    end

end
