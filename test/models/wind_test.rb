require 'test_helper'
require 'date'

class WindTest < ActiveSupport::TestCase
  setup do
    @start_date = DateTime.new(2016)
    @end_date   = DateTime.new(2019)
  end

  test "wind after" do
    start_date = DateTime.new(2018)
    winds      = Wind.after(start_date)
    assert_equal 3, winds.count
  end

  test "wind before" do
    end_date = DateTime.new(2018)
    winds    = Wind.before(end_date)
    assert_equal 2, winds.count
  end

  test "velocity directions" do
    velocities = Wind.velocities
    assert_equal [140, 240, 300], velocities.keys
  end

  test "velocity average speeds" do
    velocities = Wind.velocities
    assert_equal 45, velocities[300]
  end

  test "in date range total observations" do
    aggregation = Wind.in_date_range(@start_date, @end_date)
    assert_equal 5, aggregation[:total_observations]
  end

  test "in date range max avg speed" do
    aggregation = Wind.in_date_range(@start_date, @end_date)
    assert_equal 56, aggregation[:max_avg_speed]
  end

  test "in date range min avg speed" do
    aggregation = Wind.in_date_range(@start_date, @end_date)
    assert_equal 42, aggregation[:min_avg_speed]
  end

  test "in date range results" do
    aggregation = Wind.in_date_range(@start_date, @end_date)
    (0..35).each do |i|
      degree = i * 10
      assert_equal ({ velocity: 0.0, intensity: 0.0 }), aggregation[:results][degree] unless ([140, 240, 300].include? degree)
    end
    assert_equal ({ velocity: 56.0, intensity: 0.2 }), aggregation[:results][140]
    assert_equal ({ velocity: 42.0, intensity: 0.2 }), aggregation[:results][240]
    assert_equal ({ velocity: 45.0, intensity: 0.6 }), aggregation[:results][300]
  end

end
