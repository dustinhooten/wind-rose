require 'test_helper'
require 'json'

class WindsControllerTest < ActionDispatch::IntegrationTest

  test "should get winds index when supplied correct params" do
    get winds_url, params: {start_date: '2016-01-01', end_date: '2019-01-01'}
    assert_response :success
    body = JSON.parse(@response.body)
    assert_equal 5, body["total_observations"]
  end

  test "should return 400 error when supplied incorrect params" do
    get winds_url
    assert_response :bad_request
  end

end
