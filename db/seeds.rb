require 'csv'
require 'date'

def empty_cell?(row)
  row.any?(&:!)
end

def parse(header, cell)
  if header == :datetime
    DateTime.strptime(cell, "%Y-%m-%dT%H:%M:%S")
  else
    header == :direction and cell == '360' ? 0 : cell.to_i
  end
end

def parse_row(headers, row)
  Hash[row.map.with_index {|cell, index|
    header = headers[index].to_sym
    value = parse(header, cell)
    [header, value]
  }]
end

def parse_row_with_validation(headers, row)
  if empty_cell?(row)
    {meta: :skipped}
  else
    {meta: :success,
     wind: parse_row(headers, row)}
  end
end

def seed_data!
  directory = File.dirname(File.expand_path(__FILE__))
  file = File.join(directory, 'noaa_berthoud_pass.csv')
  line = 0
  headers = %w(station datetime direction speed)
  import_counts = {success: 0, skipped: 0}

  puts "Seeding data."
  CSV.foreach(file) do |row|
    if line > 0
      data = parse_row_with_validation(headers, row)
      import_counts[data[:meta]] += 1
      wind = data[:wind]
      Wind.create!(wind) if wind
    end
    print '.' if line % 100 == 0
    line += 1
  end
  success_count = import_counts[:success]
  skipped_count = import_counts[:skipped]
  puts "\nDone. Imported #{success_count} rows. Skipped #{skipped_count} due to missing/invalid data from NOAA."
end

seed_data!