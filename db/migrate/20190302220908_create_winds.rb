class CreateWinds < ActiveRecord::Migration[5.2]
  def change

    create_table :winds do |t|
      t.integer :station
      t.datetime :datetime
      t.integer :direction
      t.integer :speed
    end

    add_index :winds, :datetime
  end
end
