# Wind Rose

This service supplies wind data--dynamically aggregated based on query parameters--from The National Oceanic and Atmospheric Administration (NOAA).

![Demo Video](demo_video.mov)


## Design

The stack is a react app that calls to a rails api.

## Installation

1. Install RVM.
2. Install Ruby version: `2.6.1`.
3. Run `bundle install`. Dependencies are listed in the `Gemfile`
4. Run `rails db:migrate db:seed`. This might take a few minutes.
5. Install Node `v10.15.2`.
6. Install npm `v6.4.1`.
7. Start the server with `rake start`.

## Testing

1. Run `rails test` to run the entire backend test suite.
2. Run `npm run test` in the `client` directory to run the frontend test suite

